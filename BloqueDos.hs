-- 1. números primos
elimina :: Integer -> [Integer] -> [Integer]
elimina n xs =[x | x <- xs, x `mod` n /= 0]

criba :: [Integer] -> [Integer]
criba [] = []
criba (n : ns) = n : criba (elimina n ns)

primos n = criba [2..n]

-- 2. tercera letra sea "a"
letra :: String -> Char -> Bool
letra a b = a !! 2 == b || a !! 2 == 'a'

-- 3. valores repetidos en las listas
rep :: Integer -> [Integer] -> [Integer]
rep _ []=[]
rep a (b:bs)|a == b = a:rep a[]
            |otherwise = rep a (bs)

repetidos_lista :: [Integer] -> [Integer] -> [Integer]
repetidos_lista [] (ys) = []
repetidos_lista (x:xs) (ys) = k ++ repetidos_lista(xs)(ys) where k = rep x (ys)

-- 4. igualdad y diferencia de tres números 
tresIguales :: Integer -> Integer -> Integer -> Bool
tresIguales a b c = a == b && a == c


-- 5. intercalar dos listas
intercala [a,b] [c,d] = [a,c,b,d]

-- 6. permutación de números
permutacion [] = []
permutacion xs = last xs : init xs

-- 7. números abundantes
divisores :: Integer -> [Integer]
divisores x = [y | y <- [1..x-1], mod x y == 0]

abundantes :: Integer -> Bool
abundantes x = x < sum(divisores x)


-- 8. todos los nombres en la base de datos
personas ::[(String, String, Integer, Integer)]
personas = [("Cervantes","Literatura",1547,1616),
            ("Velazquez","Pintura",1599,1660),
            ("Picasso","Pintura",1881,1973),
            ("Beethoven","Musica",1770,1823),
            ("Poincare","Ciencia",1854,1912),
            ("Quevedo","Literatura",1580,1654),
            ("Goya","Pintura",1746,1828),
            ("Einsten","Ciencia",1879,1955),
            ("Mozart","Musica",1756,1791),
            ("Boticelli","Pintura",1445,1510),
            ("Borromini","Arquitectura",1599,1667),
            ("Bach","Musica",1685,1750)]

nombres :: [(String, String, Integer, Integer)] -> [String]
nombres x = [x |(x,_,_,_) <- personas]

-- 9. todos los músicos de la base de datos
musicos :: [(String, String, Integer, Integer)] -> [String]
musicos bd = [x | (x,m,_,_) <- bd, m == "Musica"]

-- 10. selecciona los nombres dependiendo su actividad
seleccion :: [(String, String, Integer, Integer)] -> String -> [String]
seleccion bd m = [ x | (x,m',_,_) <- bd, m == m' ]