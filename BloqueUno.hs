-- Ejercicio 1. Triangulos
isoceles :: Int -> Int -> Int -> Bool
isoceles a b c = if (a == b || b == c || a == c) then True else False

equilatero :: Int -> Int -> Int -> Bool
equilatero a b c = if (a == b && a == c) then True else False

pitagoras :: Int -> Int -> Int -> Bool
pitagoras a b c = if (((a ^ 2) + (b ^ 2)) == c ^ 2) then True else False

triangulo :: Int -> Int -> Int -> String
triangulo a b c = 	if (pitagoras a b c == True) then "Pitagoras" 
					else if (equilatero a b c == True) then "Equilatero" 
					else if (isoceles a b c == True) then "Isoceles" else "Escaleno"

-- ordenar de menor a mayor dos números
minimo :: Int -> Int -> String 
minimo a b =  "El menor es: "++show(min a b) ++ " el mayor es: "++show(max a b)

-- sacar la media de tres números
media x y z = (x + y + z) / 3

-- sacar el máximo de tres números
maximo :: Int -> Int -> Int -> Int
maximo x y z = if (x > y && x > z) then x else if (y > x && y > z) then y else z

-- reconocimiento de palindromos (sólo funciona con listas de enteros)
palindromo :: [Int]->Bool
palindromo b = b == reverse b

-- función que cuenta las veces que se repite el número 7
repetidos :: [Int]->Int
repetidos [] = 0
repetidos (x:xs)
	| x == 7 = 1 + repetidos(xs)
	| otherwise = repetidos(xs)

-- función que cuenta las veces que se repite cualquier número
repetidosn::[Int]->Int->Int 
repetidosn [] n=0 
repetidosn (x:xs) n|x==n=1 + repetidosn(xs) n 
     |otherwise=repetidosn(xs) n

-- divide una lista en dos: pares e impares
pares :: [Int] -> [Int]
pares b = filter even b

impares :: [Int] -> [Int]
impares b = filter odd b

pares_impares :: [Int] -> String
pares_impares b = "Pares: " ++ show(pares b) ++ " Impares: " ++ show(impares b)


-- regresa las vocales de un String
vocales :: String -> String
vocales ""=""
vocales n|k=="a"||k=="e"||k=="i"||k=="o"||k=="u"||k=="A"||k=="E"||k=="I"||k=="O"||k=="U"=k++(vocales(tail n))
		 |otherwise = "" ++ (vocales(tail n))
		 where k = take 1 n

-- valores repetidos en las listas
rep :: Int -> [Int] -> [Int]
rep _ []=[]
rep a (b:bs)|a == b = a:rep a[]
	|otherwise = rep a (bs)

repetidos_lista :: [Int] -> [Int] -> [Int]
repetidos_lista [] (ys) = []
repetidos_lista (x:xs) (ys) = k++repetidos_lista(xs)(ys) where k = rep x (ys)