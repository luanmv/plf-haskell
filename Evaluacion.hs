-- 5
suma :: [Integer] -> Integer
suma a = sum(a)

-- 4
tamano :: [Int] -> Int
tamano a = length a

-- 2
copia :: Int -> a -> [a]
copia n x = [x | _ <- [1..n]]

-- 1
maximum' :: (Ord a) => [a] -> a
maximum' [] = error "Máximo de una lista vacía"
maximum' [x] = x
maximum' (x:xs)
    | x > maxTail = x
    | otherwise   = maxTail
    where maxTail = maximum' xs

valor :: [Int] -> Int
valor a = maximum a

repn::[Int]->Int->Int 
repn [] n=0 
repn (x:xs) n|x==n=1 + repn(xs) n 
     |otherwise=repn(xs) n

cuentaMax :: [Int] -> [Int]
cuentaMax a = [valor a , repn a (valor a)]


-- 3
-- La definición por recursión es
compruebaR :: [[Int]] -> Bool
compruebaR [] = True
compruebaR (xs:xss) = tienePar xs && compruebaR xss
 
-- (tienePar xs) se verifica si xs contiene algún número par. 
tienePar  :: [Int] -> Bool
tienePar []     = False
tienePar (x:xs) = even x || tienePar xs
 
-- La definición por plegado es
compruebaP :: [[Int]] -> Bool
compruebaP = foldr f True
    where f x y = tienePar x && y
 
-- La definición por comprensión es
comprueba :: [[Int]] -> Bool
comprueba xss = and [or [even x | x <- xs] | xs <- xss]